package com.example.android.tennisscorekeeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreA;
    int scoreB;
    boolean ad_in;
    boolean deuce;
    boolean set1 = true;
    boolean set2 = false;
    boolean set3 = false;
    int pointsSet1A;
    int pointsSet2A;
    int pointsSet3A;
    int pointsSet1B;
    int pointsSet2B;
    int pointsSet3B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Displays the given score for Player A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.player_a_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Displays set 1 for Player A.
     */
    public void displaySet1A (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerAset1);
        scoreView.setText(String.valueOf("Set 1: " + score));
    }

    /**
     * Displays set 2 for Player A.
     */
    public void displaySet2A (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerAset2);
        scoreView.setText(String.valueOf("Set 2: " + score));
    }

    /**
     * Displays set 3 for Player A.
     */
    public void displaySet3A (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerAset3);
        scoreView.setText(String.valueOf("Set 3: " + score));
    }

    /**
     * Displays hidden TextVIew
     */
    public void displayHiddenText(String hiddenText) {
        final TextView scoreView = (TextView) findViewById(R.id.hiddenText);
        scoreView.setText(String.valueOf(hiddenText));
        if (hiddenText == "Deuce"){
            scoreView.setVisibility(View.VISIBLE);
            scoreView.postDelayed(new Runnable() {
                public void run() {
                    scoreView.setVisibility(View.INVISIBLE);
                }
            }, 1000);
        }

        else if (hiddenText == "")
            scoreView.setVisibility(View.INVISIBLE);

        else
            scoreView.setVisibility(View.VISIBLE);
    }

    /**
     * Displays the given score for Player B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.player_b_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Displays set 1 for Player B.
     */
    public void displaySet1B (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerBset1);
        scoreView.setText(String.valueOf("Set 1: " + score));
    }

    /**
     * Displays set 2 for Player B.
     */
    public void displaySet2B (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerBset2);
        scoreView.setText(String.valueOf("Set 2: " + score));
    }

    /**
     * Displays set 3 for Player B.
     */
    public void displaySet3B (int score) {
        TextView scoreView = (TextView) findViewById(R.id.playerBset3);
        scoreView.setText(String.valueOf("Set 3: " + score));
    }


    /**
     * Setting the Sets
     */
    public void checkSet(String player) {

        Button btnA = findViewById(R.id.pointForA);
        Button btnB = findViewById(R.id.pointForB);

        scoreA = 0;
        scoreB = 0;

        if (player == "A") {

            if (set1) {
                if (pointsSet1A < pointsSet1B+2)
                    pointsSet1A += 1;
                if (pointsSet1A == pointsSet1B+2) {
                    set1 = false;
                    set2 = true;
                }
            }

            else if (set2)  {
                if (pointsSet2A < pointsSet2B+2)
                    pointsSet2A += 1;
                if (pointsSet2A == pointsSet2B+2){
                    if (pointsSet1A == pointsSet1B+2){
                        displayHiddenText("The winner is \n Player A");
                        btnA.setEnabled(false);
                        btnB.setEnabled(false);
                    }
                    else {
                        set2 = false;
                        set3 = true;
                    }
                }
            }

            else if (set3) {
                if (pointsSet3A < pointsSet3B + 2)
                    pointsSet3A += 1;
                if (pointsSet3A == pointsSet3B + 2) {
                    displayHiddenText("The winner is \n Player A");
                    btnA.setEnabled(false);
                    btnB.setEnabled(false);
                }
            }
        }



        if (player == "B") {

            if (set1) {
                if (pointsSet1B < pointsSet1A+2)
                    pointsSet1B += 1;
                if (pointsSet1B == pointsSet1A+2) {
                    set1 = false;
                    set2 = true;
                }
            }

            else if (set2)  {
                if (pointsSet2B < pointsSet2A+2)
                    pointsSet2B += 1;
                if (pointsSet2B == pointsSet2A+2){
                    if (pointsSet1B == pointsSet1A+2){
                        displayHiddenText("The winner is \n Player B");
                        btnA.setEnabled(false);
                        btnB.setEnabled(false);
                    }
                    else {
                        set2 = false;
                        set3 = true;
                    }
                }
            }

            else if (set3) {
                if (pointsSet3B < pointsSet3A + 2)
                    pointsSet3B += 1;
                if (pointsSet3B == pointsSet3A + 2) {
                    displayHiddenText("The winner is \n Player B");
                    btnA.setEnabled(false);
                    btnB.setEnabled(false);
                }
            }
        }

        displayForTeamA(scoreA);
        displayForTeamB(scoreB);
        displaySet1A(pointsSet1A);
        displaySet2A(pointsSet2A);
        displaySet3A(pointsSet3A);
        displaySet1B(pointsSet1B);
        displaySet2B(pointsSet2B);
        displaySet3B(pointsSet3B);
    }


    /**
     * Add point for Player A
     */
    public void pointForPlayerA(View view){
        scoreA+=15;

        if (scoreA == 45) {
            scoreA = 40;
            displayForTeamA(scoreA);
        }

        else
            displayForTeamA(scoreA);

        if ((scoreA == 55) && (scoreB < 40))
            checkSet("A");

        if ((scoreA == 40) && (scoreB == 40)) {
            displayForTeamA(scoreA);
            displayHiddenText("Deuce");
        }

        else if ((scoreA == 55) && (scoreB == 40)){
            displayForTeamA(40);
            displayHiddenText("Player A: Ad-In");
        }

        else if ((scoreA == 55) && (scoreB == 55)) {
            scoreA = 40;
            scoreB = 40;
            displayForTeamA(scoreA);
            displayForTeamB(scoreB);
            displayHiddenText("Deuce");
        }

        else if ((scoreA == 70) && (scoreB == 40)) {
            displayHiddenText("");
            checkSet("A");
        }
    }

    /**
     * Add point for Player B
     */
    public void pointForPlayerB(View view){
        scoreB+=15;

        if (scoreB == 45) {
            scoreB = 40;
            displayForTeamB(scoreB);
        }

        else
            displayForTeamB(scoreB);

        if ((scoreB == 55) && (scoreA < 40))
            checkSet("B");

        if ((scoreA == 40) && (scoreB == 40)) {
            displayForTeamB(scoreB);
            displayHiddenText("Deuce");
        }

        else if ((scoreB == 55) && (scoreA == 40)){
            displayForTeamB(40);
            displayHiddenText("Player B: Ad-In");
        }
        else if ((scoreA == 55) && (scoreB == 55)) {
            scoreA = 40;
            scoreB = 40;
            displayForTeamA(scoreA);
            displayForTeamB(scoreB);
            displayHiddenText("Deuce");
        }

        else if ((scoreB == 70) && (scoreA == 40)) {
            displayHiddenText("");
            checkSet("B");
        }
    }

    public void reset(View view) {

        scoreA = 0;
        scoreB = 0;
        set1 = true;
        set2 = false;
        set3 = false;
        deuce = false;
        ad_in = false;
        pointsSet1A = 0;
        pointsSet2A = 0;
        pointsSet3A = 0;
        pointsSet1B = 0;
        pointsSet2B = 0;
        pointsSet3B = 0;
        displayHiddenText("");
        displayForTeamA(scoreA);
        displayForTeamB(scoreB);
        displaySet1A(pointsSet1A);
        displaySet2A(pointsSet2A);
        displaySet3A(pointsSet3A);
        displaySet1B(pointsSet1B);
        displaySet2B(pointsSet2B);
        displaySet3B(pointsSet3B);

        Button btnA = findViewById(R.id.pointForA);
        Button btnB = findViewById(R.id.pointForB);

        btnA.setEnabled(true);
        btnB.setEnabled(true);

    }
}